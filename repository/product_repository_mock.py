class ProductRepository:
    def __init__(self):
        None
    
    def get_all(self):
        return [
            {
                "id": 1,
                "code": "PROD04",
                "name": "Polo oscuros deportivos",
                "product_type": "Polo",
                "trademark": "Nike",
                "description": "Polo talla M",
                "sale_price": 20,
                "tags": "polo,sport,dark",
                "availability": "in stock",
                "reviews": "5",
                "status": "1"
            }
        ]
