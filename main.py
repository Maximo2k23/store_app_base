from fastapi import FastAPI
import json
from fastapi.encoders import jsonable_encoder
from repository.product_repository_mock import ProductRepository

app = FastAPI()

@app.get("/health")
def get_health():
    return {"status": "connection success"}

@app.get("/product")
def get_all(status=''):
    prod_obj = ProductRepository()
    response = prod_obj.get_all()
    return {"status": "success","data": response}

    
